// soal 1 - function hitung luas dan keliling persegi panjang
let panjang = 30;
let lebar = 20;

const luasPersegiPanjang = (panjang, lebar) => {
	return panjang * lebar;
};

const kelilingPersegiPanjang = (panjang, lebar) => {
	return 2 * (panjang + lebar);
};
// Driver code
const tampilLuas = luasPersegiPanjang(panjang, lebar);
const tampilKeliling = kelilingPersegiPanjang(panjang, lebar);

console.log(`Luas Persegi Panjang : ${tampilLuas}
Keliling Persegi Panjang : ${tampilKeliling}`);

// soal 2 - arrow function dan object literal es6
function newFunction(firstName, lastName) {
	return {
		firstName,
		lastName,
		fullName: () => {
			console.log(`${firstName} ${lastName}`);
		},
	};
}

newFunction("William", "Imoh").fullName();

// soal 3 - object destructuring
const newObject = {
	firstName: "Muhammad",
	lastName: "Iqbal Mubarok",
	address: "Jalan Ranamanyar",
	hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;
// Driver code
console.log(firstName, lastName, address, hobby);

// soal 4 - spread operator
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);

// soal 5 - template literals
const planet = "earth";
const view = "glass";
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;
console.log(before);

console.log("nama saya wildan");
filter;
