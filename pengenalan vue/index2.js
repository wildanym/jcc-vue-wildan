var vm = new Vue({
	data: {
		angka: undefined,
		jenisBilangan: "",
		p: "<p>ini adalah paragraf</p>",
		isActive: false,
		menu: "home",
		kelas: ["js", "js", "css"],
		person: {
			nama: "wildan",
			umur: 23,
		},
	},
	beforeCreate() {
		console.log("ini adalah before created");
	},
	created() {
		console.log("ini adalah created");
	},
	beforeMount() {
		console.log("ini adalah sebelum mounted");
	},
	mounted() {
		console.log("ini adalah mout");
	},
	beforeUpdate() {
		console.log("ini adalah sebelum di update");
	},
	updated() {
		console.log("ini dalah updated");
	},
	computed: {
		tampil: function () {
			this.jenisBilangan = this.hitung();
		},
	},
	methods: {
		hitung: function () {
			if (this.angka == undefined) {
				alert("masukan angka terlebih dahulu");
			} else if (this.angka.match(/^[-+]?[0-9]+$/)) {
				return (this.jenisBilangan =
					Number(this.angka) % 2 == 0 ? "Genap" : "Ganjil");
			} else {
				alert("hanya boleh input angka");
			}
		},
		darkmode: function () {
			console.log("ini dark mode");
			this.isActive = !this.isActive;
		},
	},
});
vm.$mount("#app");
